<?php


namespace App\Enums\Config;


class DefaultViewMode
{
    const GRID = "grid";
    const LLIST = "list";
}
