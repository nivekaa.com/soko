<?php


namespace App\Enums\Config;


class UserUploadsStorageMethod
{
    const LOCAL_DISK = "local_disk";
    const S3 = "amazon_s3";
    const FTP = "ftp";
    const DIGITAL_OCEAN = "digital_ocean";
    const BACKBLAZE = "backblaze";
    const DROPBOX = "black_base";
    const RACKSPACE = "rackspace";
}
