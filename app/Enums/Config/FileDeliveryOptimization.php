<?php


namespace App\Enums\Config;

/**
 * Class FileDeliveryOptimization
 * @package App\Enums\Config
 *
 * Both X-Sendfile and X-Accel need to be enabled on the server first.
 * When enabled it will reduce server memory and CPU usage when previewing or downloading files,
 * especially for large files.
 *
 */

class FileDeliveryOptimization
{
    const XACCEL = "X-Accel"; // nginx
    const XSENDFILE = "X-Sendfile"; // Apache
}
