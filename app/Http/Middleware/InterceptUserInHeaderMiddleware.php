<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Nivekaa\LaravelApiKey\Http\Middleware\AuthorizeApiKey;
use Nivekaa\LaravelApiKey\Models\ApiKey;

class InterceptUserInHeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $PERMITS = [
        "api/login",
        "api/register",
        "api/user",
        "api/apiKeys",
        "api/password/confirm",
        "api/password/reset",
        "api/password/email",
    ];

    public function handle($request, Closure $next)
    {
       $letPass = false;
       foreach ($this->PERMITS as $p) {
           if (Str::contains($request->getUri(), $p)){
               $letPass = true;
               break;
           }
       }
       if ($letPass) {
           return $next($request);
       }/**/
        $apiKeyHeader = $request->header(AuthorizeApiKey::AUTH_HEADER);
        $apiKey = ApiKey::getByKey($apiKeyHeader);

        if (!($apiKey instanceof ApiKey)) {
            return response([
                "success"=>false,
                'message' => 'Unautorized. Your API KEY is invalid'
            ], 401);
        }else{
            $userID = DB::table("user_api")
                ->where(["api_id"=>$apiKey->id])
                ->select(["*"])
                ->first();
            if (is_null($userID) || empty($userID)){
                return response([
                    "success"=>false,
                    'message' => 'User not found. Subscribe or contact our Team'
                ], 401);
            }

            $user = User::find($userID->user_id);
            if (is_null($user) || empty($user)){
                return response([
                    "success"=>false,
                    'message' => 'User not found'
                ], 401);
            }else{
                $request->merge([
                    "user"=>$user,
                    "api"=>$apiKey
                ]);
                $request->setUserResolver(function ()use ($user){
                   return $user;
                });
                return $next($request);
            }
        }
    }
}
