<?php

namespace App\Http\Controllers\API;

use App\Models\File;
use App\Models\Folder;
use App\Repositories\FileRepository;
use App\Repositories\FolderRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mimey\MimeTypes;
use Whoops\Exception\ErrorException;
use function foo\func;

/**
 * Class FileController
 * @package App\Http\Controllers\API
 */

class FileAPIController extends AppBaseController
{
    /** @var  FileRepository */
    private $fileRepository;
    private $folderRepository;

    public function __construct(FileRepository $fileRepo, FolderRepository $folderRepo)
    {
        $this->fileRepository = $fileRepo;
        $this->folderRepository = $folderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/files",
     *      summary="Get a listing of the Files.",
     *      tags={"File"},
     *      description="Get all Files with pagination. Max files per page is 25",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. default is 1",
     *         required=true,
     *         type="integer"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/File")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $files = $this->fileRepository->scopeQuery(function ($query)use ($user){
            /** @var Builder $query */
            return $query->where(["owner_id"=>$user->id]);
        })->paginate(File::MAX_PER_PAGE);

        return $this->sendResponse($files, 'Files retrieved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/files",
     *      summary="Store a newly created File in storage",
     *      tags={"File"},
     *      description="Store File",
     *      produces={"application/json"},
     *      consumes={"multipart/form-data"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *         name="file",
     *         in="formData",
     *         description="Add file",
     *         required=true,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="folder",
     *         in="formData",
     *         description="name or ID of folder",
     *         required=true,
     *         type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/File"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $this->folderRepository->skipPresenter(true);
        $this->fileRepository->skipPresenter(true);
        $user = $request->user();
        if(!$request->hasFile("file")){
            return $this->sendError("File is required");
        }
        $file = $request->file("file");
        $folderQueryParam = $request->get("folder");
        /** @var Folder $folder */
        $folder = $this->folderRepository->scopeQuery(function ($query) use ($folderQueryParam, $user){
            /** @var Builder $query */
            if (Str::isUuid($folderQueryParam))
                $query = $query->where(["id"=>$folderQueryParam]);
            else
                $query = $query
                    ->whereRaw("LOWER(name) = '". strtolower($folderQueryParam)."'")
                    ->orWhereRaw("LOWER(path) = '". strtolower($folderQueryParam)."'");
            // dd($query->toSql());
            return $query->where(["owner_id"=> $user->id]);
        })->first();

        if (empty($folder)){
            return $this->sendError("Folder param is required");
        }

        DB::beginTransaction();
        try{
            $ext_separator = ($file->getClientOriginalExtension()=="")?"":".";
            $fname = str_replace("-","",Str::uuid()->toString()).$ext_separator.$file->getClientOriginalExtension();
            $url = trim($folder->path,"/") ."/". $fname;
            $path = trim($folder->path,"/");
            $data = [
                'extension' => $file->getClientOriginalExtension(),
                'type' => $file->getType(),
                'size' => $file->getSize(),
                'filename' => $fname,
                'mime_type' => $file->getClientMimeType(),
                'category_id' => null,
                'url' => trim($url,"/"),
                'dirname' => $file->getClientOriginalName(),
                'folder_id' => $folder->id,
                'owner_id' => $user->id
            ];
            $this->folderRepository->skipPresenter(false);
            $this->fileRepository->skipPresenter(false);
            $res = $this->fileRepository->create($data);
            if ($res){
                $this->saveFile($path, $file, $fname);
                DB::commit();
            }else{
                DB::rollBack();
                return $this->sendError("Interbal error ");
            }
        }
        catch (QueryException | \Exception $e){
            DB::rollBack();
            return $this->sendError("Internal error " . $e->getMessage());
        }
        return $this->sendResponse($res, 'File saved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/files/multi/store",
     *      summary="Store many Files in storage",
     *      tags={"File"},
     *      description="Store File",
     *      produces={"application/json"},
     *      consumes={"multipart/form-data"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *         name="files[0]",
     *         in="formData",
     *         description="Add file N°1",
     *         required=true,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="files[1]",
     *         in="formData",
     *         description="Add file N°2",
     *         required=false,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="files[2]",
     *         in="formData",
     *         description="Add file N°3",
     *         required=false,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="files[3]",
     *         in="formData",
     *         description="Add file N°4",
     *         required=false,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="files[4]",
     *         in="formData",
     *         description="Add file N°5",
     *         required=false,
     *         type="file"
     *      ),
     *      @SWG\Parameter(
     *         name="folder",
     *         in="formData",
     *         description="name or ID of folder",
     *         required=true,
     *         type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/File"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function storeMultiple(Request $request)
    {
        $this->folderRepository->skipPresenter(true);
        $this->fileRepository->skipPresenter(true);
        $user = $request->user();
        if(!$request->hasFile("files")){
            return $this->sendError("Files[] is required");
        }
        // dd(count($request->file("files")));
        if (count($request->file("files"))===0){
            return $this->sendError("At least of one file is required");
        }
        if (count($request->file("files")) > File::MAX_FILES_UPLOADED) {
            return $this->sendError("Max files to upload is ". File::MAX_FILES_UPLOADED);
        }
        $files = $request->file("files");
        $folderQueryParam = $request->get("folder");
        /** @var Folder $folder */
        $folder = $this->folderRepository->scopeQuery(function ($query) use ($folderQueryParam, $user){
            /** @var Builder $query */
            if (Str::isUuid($folderQueryParam))
                $query = $query->where(["id"=>$folderQueryParam]);
            else
                $query = $query
                    ->whereRaw("LOWER(name) = '". strtolower($folderQueryParam)."'")
                    ->orWhereRaw("LOWER(path) = '". strtolower($folderQueryParam)."'");
            // dd($query->toSql());
            return $query->where(["owner_id"=> $user->id]);
        })->first();

        if (empty($folder)){
            return $this->sendError("Folder param is required");
        }

        $data = [];
        $newFileIds = [];
        DB::beginTransaction();
        try{
            $this->fileRepository->skipPresenter(true);
            foreach($files as $key => $file) {
                $ext_separator = ($file->getClientOriginalExtension()=="")?"":".";
                $fname = str_replace("-","",Str::uuid()->toString()).$ext_separator.$file->getClientOriginalExtension();
                $url = trim($folder->path,"/") ."/". $fname;
                $path = trim($folder->path,"/");
                $data[$key] = [
                    'extension' => $file->getClientOriginalExtension(),
                    'type' => $file->getType(),
                    'size' => $file->getSize(),
                    'filename' => $fname,
                    'mime_type' => $file->getClientMimeType(),
                    'category_id' => null,
                    'url' => $url,
                    'dirname' => $file->getClientOriginalName(),
                    'folder_id' => $folder->id,
                    'owner_id' => $user->id
                ];
                $record = $this->fileRepository->create($data[$key]);
                $data[$key] = $record;
                $newFileIds[] = $record["id"];
                if ($data[$key]){
                    $this->saveFile($path, $file, $fname);
                }
            }
            DB::commit();
        }
        catch (QueryException | \ErrorException | ErrorException | \Exception $e){
            DB::rollBack();
            if (count($data)){
                foreach ($data as $dat){
                    $this->deleteFile($dat["url"]);
                }
            }
            Log::error($e->getMessage());
            return $this->sendError("Internal error ".  $e->getMessage());
        }
        $this->fileRepository->skipPresenter(false);
        $presents = $this->fileRepository->findWhereIn("id", $newFileIds);

        return $this->sendResponse($presents , 'Files saved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/files/store/base64",
     *      summary="Store a newly created File base64 encoded in storage",
     *      tags={"File"},
     *      description="Store File",
     *      produces={"application/json"},
     *      consumes={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="File object that needs to be added to the store",
     *         required=true,
     *         @SWG\Schema(
     *              @SWG\Property(
     *                  property="file",
     *                  description="File encoded in base64",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="folder",
     *                  description="Folder",
     *                  type="string"
     *              ),
     *           ),
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/File"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function storeBase64(Request $request){
        $this->folderRepository->skipPresenter(true);
        $this->fileRepository->skipPresenter(true);
        $user = $request->user();
        if(!$request->has("file")){
            return $this->sendError("File is required");
        }
        $base64_fille = $request->get("file");
        $folderQueryParam = $request->get("folder");
        /** @var Folder $folder */
        $folder = $this->folderRepository->scopeQuery(function ($query) use ($folderQueryParam, $user){
            /** @var Builder $query */
            if (Str::isUuid($folderQueryParam))
                $query = $query->where(["id"=>$folderQueryParam]);
            else
                $query = $query->whereRaw("LOWER(name) = '". strtolower($folderQueryParam)."'")
                    ->orWhereRaw("LOWER(path) = '". strtolower($folderQueryParam)."'");
            return $query->where(["owner_id"=> $user->id]);
        })->first();

        if (empty($folder)){
            return $this->sendError("Folder param is required");
        }
        $validB64 = preg_match_all("/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).base64,.*/", $base64_fille);
        if ($validB64 && strpos($base64_fille, ',') !== false) {
            @list($encode, $base64string) = explode(',', $base64_fille);
        }else{
            return $this->sendError("Invalid base64 file encoded");
        }

        DB::beginTransaction();
        try{

            $y = 1;
            if (Str::endsWith($base64_fille, "==")){ $y = 2; }
            $size = (strlen(rtrim($base64_fille, '=')) * 3 / 4) - $y;
            $mimeType = explode(";", substr($encode, 5, strlen($encode)))[0];
            $mime = new MimeTypes();
            $extension = $mime->getExtension($mimeType);
            $type = explode("/", $mimeType)[1];

            $fname = str_replace("-","",Str::uuid()->toString()).".".$extension;
            $url = trim($folder->path,"/") ."/". $fname;
            $path = trim($folder->path,"/");
            $file = base64_decode($base64string, true);

            $data = [
                'extension' => $extension,
                'type' => "file",
                'size' => $size,
                'filename' => $fname,
                'mime_type' => $type,
                'category_id' => null,
                'url' => $url,
                'dirname' => $fname,
                'folder_id' => $folder->id,
                'owner_id' => $user->id
            ];

            $this->fileRepository->skipPresenter(false);
            $res = $this->fileRepository->create($data);
            if ($res){
                $this->saveFile($path, $file, $fname,true);
                DB::commit();
            }else{
                DB::rollBack();
                return $this->sendError("Interbal error ");
            }
        }
        catch (QueryException | \Exception $e){
            DB::rollBack();
            return $this->sendError("Internal error : " . $e->getCode());
        }
        return $this->sendResponse($res, 'File saved successfully');
    }

    private function saveFile($path, $file, $filename, $b64=false){
        if ($b64)
            return Storage::disk("soko")->put("$path/$filename", $file);
        return Storage::disk("soko")->putFileAs($path, $file, $filename);
    }

    private function deleteFile($path): bool {
        if (!Storage::disk("soko")->exists($path))
            return false;
        return Storage::disk("soko")->delete($path);
    }

    private function fileExist($path): bool {
        return Storage::disk("soko")->exists($path);
    }

    /**
     * @param $id_of_name
     * @return Response
     *
     * @SWG\Get(
     *      path="/files/{id}",
     *      summary="Display the specified File",
     *      tags={"File"},
     *      description="Get File. The `$id` can be :
     *      UUID File ID
     *      or file name : http://domain.com/soko/..ANOTHER_WAY../{filename} (filename: e.g: dqsdqsdqe.png, aezrzerzer.pdf, lkfkmzoj.txt, etc...)
     *      or path : http://domain.com/soko/{path} here the path variable must be base64 encoded. This is cause of `/` may contain the PATH,
     *      or the orginal filename (e.g: foo.txt, foo bar.pdf, etc...)",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of File",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/File"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id_of_name)
    {
        $user = \request()->user();
        $this->fileRepository->skipPresenter(false);
        /** @var File $file */
        $file = $this->fileRepository->scopeQuery(function ($query) use ($user, $id_of_name){

            /** @var Builder $query */
            if (Str::isUuid($id_of_name))
                $query = $query->where(["id"=>$id_of_name]);
            else
                $query = $query
                    ->whereRaw("LOWER(filename) = '". strtolower($id_of_name)."'")
                    ->orwhereRaw("LOWER(dirname) = '". strtolower($id_of_name)."'")
                    ->orWhereRaw("LOWER(url) = '". strtolower(base64_decode($id_of_name))."'");
            return $query->where(["owner_id"=> $user->id]);
        })->first();
        if (empty($file)) {
            return $this->sendError('File not found');
        }
        $this->fileRepository->skipPresenter(false);
        return $this->sendResponse($file, null);
    }

    /**
     * @param $_folfer
     * @return Response
     *
     * @SWG\Get(
     *      path="/files/folder/{folder}/show",
     *      summary="Display the files in specified folder",
     *      tags={"File"},
     *      description="Get Files. The `$folder` can be :
     *      UUID Folder ID
     *      or folder name
     *      or path : http://domain.com/soko/{path}/{...files_contening...} here the path variable must be base64 encoded. This is cause of `/` may contain the PATH,
     *      or the orginal filename (e.g: foo.txt, foo bar.pdf, etc...)<br/>with pagination. Max files per page is 25",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Page number. default is 1",
     *         required=true,
     *         type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="folder",
     *          description="Folder name or ID",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/File")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function showInFolder($_folfer)
    {
        $user = \request()->user();
        $this->folderRepository->skipPresenter(true);
        $this->fileRepository->skipPresenter(true);

        /** @var Folder $folfer */
        $folfer = $this->folderRepository->scopeQuery(function ($query) use ($user, $_folfer){
            /** @var Builder $query */
            if (Str::isUuid($_folfer))
                $query = $query->where(["id"=>$_folfer]);
            else
                $query = $query
                    ->whereRaw("LOWER(name) = '". strtolower($_folfer)."'")
                    ->orWhereRaw("LOWER(path) = '". strtolower(base64_decode($_folfer))."'");
            return $query->where(["owner_id"=> $user->id]);
        })->first();
        if (empty($folfer)) {
            return $this->sendError('Folder not found');
        }
        $this->fileRepository->skipPresenter(false);
        $files = $this->fileRepository->scopeQuery(function ($q) use ($folfer, $user){
            return $q->where([
                "folder_id"=>$folfer->id,
                "owner_id"=>$user->id]
            );
        })->paginate(File::MAX_PER_PAGE);
        return $this->sendResponse($files, null);
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/files/{id}",
     *      summary="Remove the specified File from storage",
     *      tags={"File"},
     *      description="Delete File",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id (UUID) of File",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = \request()->user();
        $this->fileRepository->skipPresenter(true);
        /** @var File $file */
        $file = $this->fileRepository->scopeQuery(function ($query) use ($user, $id){
            /** @var Builder $query */
            return $query->where([
                "owner_id"=>$user->id,
                "id"=>$id
            ]);
        })->first();

        if (empty($file)) {
            return $this->sendError('File not found');
        }
        $file->delete();
        if ($this->fileExist($file->url)){
            $this->deleteFile($file->url);
        }else{
            return $this->sendError("File not found in dir");
        }
        return $this->sendSuccess('File is successfully deleted');
    }
}
