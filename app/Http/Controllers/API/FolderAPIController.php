<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFolderAPIRequest;
use App\Http\Requests\API\UpdateFolderAPIRequest;
use App\Models\Folder;
use App\Models\User;
use App\Repositories\FolderRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class FolderController
 * @package App\Http\Controllers\API
 */

class FolderAPIController extends AppBaseController
{
    /** @var  FolderRepository */
    private $folderRepository;

    public function __construct(FolderRepository $folderRepo)
    {
        $this->folderRepository = $folderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/folders",
     *      summary="Get a listing of the Folders.",
     *      tags={"Folder"},
     *      description="Get all Folders",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Folder")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $folders = $this->folderRepository->scopeQuery(function ($query)use ($user){
            return $query->where(["owner_id"=>$user->id]);
        })->paginate();

        return $this->sendResponse($folders, 'Folders retrieved successfully');
    }

    /**
     * @param CreateFolderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/folders",
     *      summary="Store a newly created Folder in storage",
     *      tags={"Folder"},
     *      description="Store Folder",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Folder that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Folder"),
     *          @SWG\Property(
     *              property="parent",
     *              description="parent folder",
     *              type="string"
     *          ),
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Folder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws ValidatorException
     */
    public function store(CreateFolderAPIRequest $request)
    {
        /** @var User $user */
        $user = $request->user();
        $input = $request->all();
        $input["owner_id"] = $request->user()->id;
        $path = "";
        $this->folderRepository->skipPresenter(true);
        if ($request->has("parent")){
            $p = $request->get("parent");
            $parent = $this->folderRepository->scopeQuery(function ($query) use ($user, $p){
                /** @var Builder $query */
                if (Str::isUuid($p))
                    $query = $query->where(["id"=>$p]);
                else
                    $query = $query
                        ->whereRaw("LOWER(name) = '". strtolower($p)."'")
                        ->orWhereRaw("LOWER(path) = '". strtolower($p)."'");

                // dd($query->toSql());
                return $query->where(["owner_id"=> $user->id]);
            })->first();
            if (empty($parent)){
                return  $this->sendError("Parent folder $p not exist");
            }
            $input["parent_id"] = $parent->id;
            $path .= trim($parent->path,"/") . "/" . trim($input["name"],"/");
        }else{
            $path = $user->id . "/" . trim($input["name"], "/");
        }
        $input["path"] = $path;
        $checkexist = Folder::query()->where(["path"=>$path])->get();
        if ($checkexist->count()){
            return $this->sendError("This folder exist.");
        }

        $this->folderRepository->skipPresenter(false);
        $folder = $this->folderRepository->create($input);
        if ($folder){
            $this->createDir($user->id,$path);
        }else
            return $this->sendError("Folder not created");
        return $this->sendResponse($folder, 'Folder saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/folders/{id}",
     *      summary="Display the specified Folder",
     *      tags={"Folder"},
     *      description="Get Folder",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Folder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Folder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Folder $folder */
        $folder = $this->folderRepository->find($id);

        if (empty($folder)) {
            return $this->sendError('Folder not found');
        }

        return $this->sendResponse($folder, 'Folder retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/folders/{id}",
     *      summary="Remove the specified Folder from storage",
     *      tags={"Folder"},
     *      description="Delete Folder",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Folder",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->folderRepository->skipPresenter(true);
        /** @var Folder $folder */
        $folder = $this->folderRepository->find($id);
        if (empty($folder)) {
            return $this->sendError('Folder not found');
        }

        $folder->delete();
        $user = \request()->user();
        $this->removeDir($user->id, $folder->path);
        return $this->sendSuccess('Folder deleted successfully');
    }

    public function createDir($user_id, $name){
        $dirname = "$user_id/$name";
        Storage::disk("soko")->makeDirectory($dirname);
    }

    public function removeDir($user_id, $path){
        Storage::disk("soko")->deleteDirectory("$user_id/$path");
    }

    public function dirExist($user_id, $path){
        return Storage::disk("soko")->directories("$user_id/$path");
    }
}
