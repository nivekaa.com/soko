<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePlanAPIRequest;
use App\Http\Requests\API\UpdatePlanAPIRequest;
use App\Models\Plan;
use App\Repositories\PlanRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Response;

/**
 * Class PlanController
 * @package App\Http\Controllers\API
 */

class PlanAPIController extends AppBaseController
{
    /** @var  PlanRepository */
    private $planRepository;

    public function __construct(PlanRepository $planRepo)
    {
        $this->planRepository = $planRepo;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/plans/{id}",
     *      summary="Display the specified Plan",
     *      tags={"Plan"},
     *      description="Get Plan",
     *      produces={"application/json"},
     *      security={{"default": {}}},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        // $user = \request()->user();
        /** @var Plan $plan */
        $plan = $this->planRepository->scopeQuery(function ($query) use ($user){
            /** @var Builder $query */
            return $query;
        })->find($id);

        if (empty($plan)) {
            return $this->sendError('Plan not found');
        }
        return $this->sendResponse($plan, null);
    }
}
