<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\CreateApiKeyRequest;
use App\Models\User;
use App\Repositories\ApiKeyRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Nivekaa\LaravelApiKey\Models\ApiKey;

class ApiKeyAPIController extends AppBaseController
{
    /** @var  ApiKeyRepository */
    private $apiKeyRepository;
    private $userRepository;

    public function __construct(ApiKeyRepository $apiKeyRepo, UserRepository $userrepo)
    {
        $this->apiKeyRepository = $apiKeyRepo;
        $this->userRepository = $userrepo;
    }

    /**
     * Display a listing of the ApiKey.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $apiKeys = $user->apis();
        return $this->sendResponse($apiKeys, null);
    }

    /**
     * Store a newly created ApiKey in storage.
     *
     * @param CreateApiKeyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateApiKeyRequest $request)
    {
        $input = $request->all();
        /** @var User $user */
        $this->userRepository->skipPresenter(true);
        $user = $this->userRepository->find($input["user_id"]);
        //dd($user);
        if ($user->max_api <= count($user->apis)) {
            return $this->sendError("You have attempt max API", 403);
        }

        $data = [
            "name"=>$input["name"],
            "key"=> ApiKey::generate(),
        ];
        $apiKey = $this->apiKeyRepository->create($data);

        $user->apis()->attach($apiKey->id);

        return $this->sendResponse($apiKey, "new Api Key successfully added.");
    }
    public function active($id, Request $request)
    {
        $input = $request->all();
        /** @var ApiKey $api */
        $apiKey = $this->apiKeyRepository->find($id);
        if (empty($apiKey)) {
            return $this->sendError("Api Key not found", 404);
        }

        /** @var User $user */
        $user = $this->userRepository->find($input["user_id"]);

        $action = $input["active"]?"activated":"desactivated";
        DB::beginTransaction();
        try{
            $apiKey = $this->apiKeyRepository->update(["active"=>boolval($input["active"])], $id);
            DB::commit();
            return $this->sendSuccess('Api Key '. $action .' successfully.');
        } catch (QueryException | \Exception $e){
            DB::rollBack();
            return $this->sendError("Internal Error", 500);
        }
    }

    /**
     * Remove the specified ApiKey from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApiKey $apiKey */
        $apiKey = $this->apiKeyRepository->find($id);
        if (empty($apiKey)) {
            return $this->sendError("Api Key not found", 500);
        }

        DB::beginTransaction();
        try{
            $this->apiKeyRepository->delete($id);
            /** @var User $user */
            $user = auth()->user();
            $user->apis()->detach($id);
            DB::commit();
        } catch (QueryException | \Exception $e){
            DB::rollBack();
            return $this->sendError("Internal error. try later", 500);
        }
        return $this->sendSuccess('Api Key deleted successfully.');
    }
}
