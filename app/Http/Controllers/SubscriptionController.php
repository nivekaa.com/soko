<?php

namespace App\Http\Controllers;

use App\Repositories\PlanRepository;
use Illuminate\Http\Request;

class SubscriptionController extends AppBaseController
{
    private $planRepository;

    public function __construct(PlanRepository $planRepo)
    {
        $this->planRepository = $planRepo;
    }

    public function pricing(Request $request){
        $plan = $this->planRepository->all();

        return view("subscription.pricing", compact($plan));
    }
}
