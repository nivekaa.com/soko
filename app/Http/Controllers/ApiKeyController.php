<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApiKeyRequest;
use App\Http\Requests\UpdateApiKeyRequest;
use App\Models\User;
use App\Repositories\ApiKeyRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Response;
use Nivekaa\LaravelApiKey\Models\ApiKey;

class ApiKeyController extends AppBaseController
{
    /** @var  ApiKeyRepository */
    private $apiKeyRepository;
    private $userRepository;

    public function __construct(ApiKeyRepository $apiKeyRepo, UserRepository $userrepo)
    {
        $this->apiKeyRepository = $apiKeyRepo;
        $this->userRepository = $userrepo;
    }

    /**
     * Display a listing of the ApiKey.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $apiKeys = $this->apiKeyRepository->all();

        return view('api_keys.index')
            ->with('apiKeys', $apiKeys);
    }

    /**
     * Show the form for creating a new ApiKey.
     *
     * @return Response
     */
    public function create()
    {
        return view('api_keys.create');
    }

    /**
     * Store a newly created ApiKey in storage.
     *
     * @param CreateApiKeyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CreateApiKeyRequest $request)
    {
        $input = $request->all();
        /** @var User $user */
        $this->userRepository->skipPresenter(true);
        $user = $this->userRepository->find($input["user_id"]);
        //dd($user);
        if ($user->max_api <= count($user->apis)) {
            if ($request->ajax()){
                return $this->sendError("You have attempt max API", 403);
            }else
            {
                Flash::error('You have attempt max API.');
                return redirect(route('home'));
            }
        }

        $data = [
            "name"=>$input["name"],
            "key"=> ApiKey::generate(),
        ];
        $apiKey = $this->apiKeyRepository->create($data);

        $user->apis()->attach($apiKey->id);

        if ($request->ajax()){
            return $this->sendResponse($apiKey, "new Api Key successfully added.");
        }
        Flash::success('Api Key saved successfully.');
        return redirect(route('home'));
    }
    public function active($id, Request $request)
    {
        $input = $request->all();
        /** @var ApiKey $api */
        $apiKey = $this->apiKeyRepository->find($id);
        if (empty($apiKey)) {
            if ($request->ajax())
                return $this->sendError("Api Key not found", 404);
            Flash::error('Api Key not found');
            return redirect(route('home'));
        }

        /** @var User $user */
        $user = $this->userRepository->find($input["user_id"]);

        $action = $input["active"]?"activated":"desactivated";
        DB::beginTransaction();
        try{
            $apiKey = $this->apiKeyRepository->update(["active"=>boolval($input["active"])], $id);
            DB::commit();
            if ($request->ajax())
                return $this->sendSuccess('Api Key '. $action .' successfully.');
            Flash::success('Api Key '. $action .' successfully.');
            return redirect(route('home'));
        } catch (QueryException | \Exception $e){
            DB::rollBack();
            if ($request->ajax())
                return $this->sendError("Internal Error", 500);
            Flash::error("Internal error");
            return redirect(route('home'));
        }
    }

    /**
     * Display the specified ApiKey.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $apiKey = $this->apiKeyRepository->find($id);

        if (empty($apiKey)) {
            Flash::error('Api Key not found');

            return redirect(route('apiKeys.index'));
        }

        return view('api_keys.show')->with('apiKey', $apiKey);
    }

    /**
     * Show the form for editing the specified ApiKey.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $apiKey = $this->apiKeyRepository->find($id);

        if (empty($apiKey)) {
            Flash::error('Api Key not found');

            return redirect(route('apiKeys.index'));
        }

        return view('api_keys.edit')->with('apiKey', $apiKey);
    }

    /**
     * Update the specified ApiKey in storage.
     *
     * @param int $id
     * @param UpdateApiKeyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApiKeyRequest $request)
    {
        $apiKey = $this->apiKeyRepository->find($id);

        if (empty($apiKey)) {
            Flash::error('Api Key not found');

            return redirect(route('apiKeys.index'));
        }

        $apiKey = $this->apiKeyRepository->update($request->all(), $id);

        Flash::success('Api Key updated successfully.');

        return redirect(route('apiKeys.index'));
    }

    /**
     * Remove the specified ApiKey from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApiKey $apiKey */
        $apiKey = $this->apiKeyRepository->find($id);
        if (empty($apiKey)) {
            Flash::error('Api Key not found');
            return redirect(route('home'));
        }

        DB::beginTransaction();
        try{
            $this->apiKeyRepository->delete($id);
            /** @var User $user */
            $user = auth()->user();
            $user->apis()->detach($id);
            DB::commit();
        }
        catch (QueryException | \Exception $e){
            DB::rollBack();
            Flash::error("Internal error. try later");
            return redirect(route("home"));
        }
        Flash::success('Api Key deleted successfully.');
        return redirect(route('home'));
    }
}
