<?php

namespace App\Http\Controllers;

use App\Utils\ResponseUtil;
use Illuminate\Support\Facades\Response;

/**
 * @SWG\Swagger(
 *   basePath="/storage/api/v1",
 *   @SWG\Info(
 *     title="``SOKO`` FILES",
 *     version="1.0.0",
 *     description="Microservice permettant le stockage des fichier par le protocol HTTP
 * header `X-API-KEY` is required for all request",
 *     license="Apache-2.0",
 *     contact={
 *          "email": "contact@nivekaa.com",
 *          "name": "KEVIN KEMTA"
 *      }
 *   ),
 *   @SWG\SecurityScheme(
 *      securityDefinition="default",
 *      type="apiKey",
 *      in="header",
 *      name="X-API-KEY"
 *    )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
