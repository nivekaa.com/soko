<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDescriptionPlanLineRequest;
use App\Http\Requests\UpdateDescriptionPlanLineRequest;
use App\Repositories\DescriptionPlanLineRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Response;

class DescriptionPlanLineController extends AppBaseController
{
    /** @var  DescriptionPlanLineRepository */
    private $descriptionPlanLineRepository;

    public function __construct(DescriptionPlanLineRepository $descriptionPlanLineRepo)
    {
        $this->descriptionPlanLineRepository = $descriptionPlanLineRepo;
    }

    /**
     * Display a listing of the DescriptionPlanLine.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $descriptionPlanLines = $this->descriptionPlanLineRepository->all();

        return view('description_plan_lines.index')
            ->with('descriptionPlanLines', $descriptionPlanLines);
    }

    /**
     * Show the form for creating a new DescriptionPlanLine.
     *
     * @return Response
     */
    public function create()
    {
        return view('description_plan_lines.create');
    }

    /**
     * Store a newly created DescriptionPlanLine in storage.
     *
     * @param CreateDescriptionPlanLineRequest $request
     *
     * @return Response
     */
    public function store(CreateDescriptionPlanLineRequest $request)
    {
        $input = $request->all();

        $descriptionPlanLine = $this->descriptionPlanLineRepository->create($input);

        Flash::success('Description Plan Line saved successfully.');

        return redirect(route('descriptionPlanLines.index'));
    }

    /**
     * Display the specified DescriptionPlanLine.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $descriptionPlanLine = $this->descriptionPlanLineRepository->find($id);

        if (empty($descriptionPlanLine)) {
            Flash::error('Description Plan Line not found');

            return redirect(route('descriptionPlanLines.index'));
        }

        return view('description_plan_lines.show')->with('descriptionPlanLine', $descriptionPlanLine);
    }

    /**
     * Show the form for editing the specified DescriptionPlanLine.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $descriptionPlanLine = $this->descriptionPlanLineRepository->find($id);

        if (empty($descriptionPlanLine)) {
            Flash::error('Description Plan Line not found');

            return redirect(route('descriptionPlanLines.index'));
        }

        return view('description_plan_lines.edit')->with('descriptionPlanLine', $descriptionPlanLine);
    }

    /**
     * Update the specified DescriptionPlanLine in storage.
     *
     * @param int $id
     * @param UpdateDescriptionPlanLineRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDescriptionPlanLineRequest $request)
    {
        $descriptionPlanLine = $this->descriptionPlanLineRepository->find($id);

        if (empty($descriptionPlanLine)) {
            Flash::error('Description Plan Line not found');

            return redirect(route('descriptionPlanLines.index'));
        }

        $descriptionPlanLine = $this->descriptionPlanLineRepository->update($request->all(), $id);

        Flash::success('Description Plan Line updated successfully.');

        return redirect(route('descriptionPlanLines.index'));
    }

    /**
     * Remove the specified DescriptionPlanLine from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $descriptionPlanLine = $this->descriptionPlanLineRepository->find($id);

        if (empty($descriptionPlanLine)) {
            Flash::error('Description Plan Line not found');

            return redirect(route('descriptionPlanLines.index'));
        }

        $this->descriptionPlanLineRepository->delete($id);

        Flash::success('Description Plan Line deleted successfully.');

        return redirect(route('descriptionPlanLines.index'));
    }
}
