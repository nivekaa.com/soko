<?php

namespace App\Presenters;

use App\Transformers\FolderTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class FolderPresenter.
 *
 * @package namespace App\Presenters;
 */
class FolderPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new FolderTransformer();
    }
}
