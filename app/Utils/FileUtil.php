<?php


namespace App\Utils;


class FileUtil
{

    public static function convert_filesize($size, $decimals = 2){
        $units = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        return sprintf("%.{$decimals}f ",round($size, $decimals)).$units[$i];
    }

    public function isImageBase64(string  $imageEncode){
        return preg_match('/^data:image\/(\w+);base64,/', $imageEncode);
    }

    public static function strigToBinary($string)
    {
        $characters = str_split($string);

        $binary = [];
        foreach ($characters as $character) {
            $data = unpack('H*', $character);
            $binary[] = base_convert($data[1], 16, 2);
        }

        return implode(' ', $binary);
    }

    public static function binaryToString($binary)
    {
        $binaries = explode(' ', $binary);

        $string = null;
        foreach ($binaries as $binary) {
            $string .= pack('H*', dechex(bindec($binary)));
        }

        return $string;
    }

}
