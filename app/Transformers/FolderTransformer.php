<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Folder;

/**
 * Class FolderTransformer.
 *
 * @package namespace App\Transformers;
 */
class FolderTransformer extends TransformerAbstract
{
    /**
     * Transform the Folder entity.
     *
     * @param Folder $model
     *
     * @return array
     */
    public function transform(Folder $model)
    {
        $data = [
            '_id' => (string) $model->id,
            'name' => $model->name,
            'base' => $model->is_base,
            'created_at' => $model->created_at->format("Y-m-d H:i:s"),
            /*'created_by' => [
                //"id"=> $model->owner->id,
                "name"=> $model->owner->name??"",
            ]*/
        ];

        if (!is_null($model->parent)){
           $data =  array_merge($data,
                [
                    'parent' => [
                        "_id" => $model->parent->id??"",
                        "name" => $model->parent->name??"",
                    ],
                ]
            );
        }else{
            $data["parent"] = null;
        }
        return $data;
    }
}
