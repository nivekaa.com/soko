<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the User entity.
     *
     * @param User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'         => (int) $model->id,
            'name' =>  $model->name,
            'email' =>  $model->email,
            'max_api' => $model->max_api,
            'space_left' => $model->space_left,
            'number_files' =>$model->number_files,
            'created_at' => $model->created_at->format("Y-m-d H:i:s"),
            'updated_at' => $model->updated_at->format("Y-m-d H:i:s")
        ];
    }
}
