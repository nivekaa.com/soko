<?php

namespace App\Transformers;

use App\Utils\FileUtil;
use League\Fractal\Scope;
use League\Fractal\TransformerAbstract;
use App\Models\File;

/**
 * Class FileTransformer.
 *
 * @package namespace App\Transformers;
 */
class FileTransformer extends TransformerAbstract
{

    /**
     * Transform the File entity.
     *
     * @param File $model
     *
     * @return array
     */


    public function transform(File $model)
    {
        return [
            '_id' => (string) $model->id,
            'extension' => $model->extension,
            //'type' => $model->type,
            'size' => FileUtil::convert_filesize($model->size, 3),
            'original_name' => $model->dirname,
            //'mime_type' => $model->mime_type,
            'url' => url("soko/".$model->url),
            'created_at' => $model->created_at->format("Y-m-d H:i:s"),
            'folder' => [
                //"id" => $model->folder->id??"",
                "name" => $model->folder->name??"",
            ],
            /*'created_by' => [
                //"id"=> $model->owner->id,
                "name"=> $model->owner->name??"",
            ]*/
        ];
    }

}
