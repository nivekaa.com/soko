<?php


namespace App\Models;

/**
 * @SWG\Definition(
 *      definition="ApiKey",
 *      required={"name"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="api_key",
 *          description="api key valeur 64 bits",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ApiKey extends \Nivekaa\LaravelApiKey\Models\ApiKey
{

}
