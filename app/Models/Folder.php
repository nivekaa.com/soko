<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;

/**
 * @SWG\Definition(
 *      definition="Folder",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent",
 *          description="parent id or parent name folder",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_base",
 *          description="is base folders of owner ID",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Folder extends Model
{
    use SoftDeletes;
    use UsesUuid;

    public $table = 'folder';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'parent_id',
        'path',
        'is_base',
        'owner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'path' => 'string',
        'is_base' => 'boolean',
        'parent_id' => 'string',
        'owner_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => ['required'/*,"unique:folder"*/],
        //'owner_id' => 'required'
    ];


    /**
     * @return BelongsTo
     **/
    public function parent()
    {
        return  $this->belongsTo(Folder::class, 'parent_id');
    }

    /**
     * @return BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return HasMany
     **/
    public function files()
    {
        return $this->hasMany(File::class, 'foolder_id');
    }
}
