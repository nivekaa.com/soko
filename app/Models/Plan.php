<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Plan",
 *      required={"name","price","interval","interval_count"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="promotional_price",
 *          description="promotional_price",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="interval",
 *          description="interval",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="interval_count",
 *          description="interval_count",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */

/**
 * Class Plan
 * @package App\Models
 *
 * @property string $name
 * @property float $price
 * @property float $promotional_price
 * @property integer $internal
 * @property integer $interval_count
 */
class Plan extends Model
{
    use SoftDeletes;

    public $table = 'plan';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'price',
        'promotional_price',
        'interval',
        'interval_count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'price' => 'float',
        'promotional_price' => 'float',
        'interval' => 'string',
        'interval_count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'price' => 'required',
        'promotional_price' => 'required',
        'interval' => 'required',
        'interval_count' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function descriptionLines()
    {
        return $this->hasMany(\App\Models\DescriptionPlanLine::class, 'plan_id');
    }
}
