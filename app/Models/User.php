<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Nivekaa\LaravelApiKey\Models\ApiKey;


/**
 * @SWG\Definition(
 *      definition="User",
 *      required={"name"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="max_api",
 *          description="max keys api to created",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="space_left",
 *          description="Space left to allow for user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="number_files",
 *          description="max files to store",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */



/**
 * Class User
 * @package App\Models
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $max_api
 * @property integer $space_left
 * @property integer $number_files
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;
    const DEFAULT_MAX_API = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'max_api',
        'space_left',
        'number_files'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return BelongsToMany
     **/
    public function apis()
    {
        return $this->belongsToMany(ApiKey::class,
            'user_api',
            'user_id',
            'api_id');
    }

    /**
     * @return Builder|Model|object|Folder
     **/
    public function baseFolder()
    {
        return Folder::query()
            ->where([
                "is_base"=>1,
                "owner_id"=>$this->id
            ])->first();
    }

    public function createBaseFolder($apiName){
        $name = $apiName . "---" .Str::uuid()->toString();
        Folder::query()
            ->create([
                "owner_id"=>$this->id,
                "is_base"=>1,
                "path"=>$name,
                "name"=>$apiName
            ])->get();
    }
}
