<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="File",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="the UUID",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extension",
 *          description="the extension of file (e.g: png, zip, txt, etct...)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type of file (e.g: file, image, zip, etct...)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="size",
 *          description="the size for file",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="filename",
 *          description="filename",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mime_type",
 *          description="mime type (e.g: application/zip, image/png, etc...)",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="category_id",
 *          description="category IDd",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="owner_id",
 *          description="owner ID: the current user by API KEY",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="folder",
 *          description="folder UUID id or name or path",
 *          type="string"
 *      )
 * )
 */
class File extends Model
{
    use SoftDeletes;
    use UsesUuid;

    const MAX_FILES_UPLOADED = 16;
    const MAX_PER_PAGE = 25;
    public $table = 'file';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'extension',
        'type',
        'size',
        'filename',
        'mime_type',
        'category_id',
        'url',
        'owner_id',
        'dirname',
        'folder_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'extension' => 'string',
        'type' => 'string',
        'size' => 'integer',
        'owner_id' => 'integer',
        'filename' => 'string',
        'mime_type' => 'string',
        'category_id' => 'string',
        'url' => 'string',
        'dirname' => 'string',
        'folder_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'filename' => 'required'
        'folder' => 'required'
    ];

    /**
     * @return BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return BelongsTo
     **/
    public function folder()
    {
        return $this->belongsTo(Folder::class, 'folder_id');
    }

}
