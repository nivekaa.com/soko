<?php

namespace App\Exceptions;

use App\Utils\ResponseUtil;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        if ($this->isHttpException($e)) {
            return $this->renderHttpException($e);
        }

        $msg = null;

        if ($e instanceof TokenMismatchException) {
            $msg = 'Your session token had expired. Please try again.';
        } else if ($e instanceof UnauthorizedException) {
            $msg = 'You are not authorized to perform the previous action.';
        } else if ($e instanceof QueryException) {
            $msg = 'You are not authorized to perform the previous action.';
        } else if ($e instanceof FileNotFoundException) {
            $msg = 'File not found';
        }else if ($e instanceof BadRequestHttpException) {
            $msg = 'Bad request';
        }/*else{
            $msg = "Internal error. Try later";
        }*/

        if (!is_null($msg)) {
            if ($request->ajax()) {
                // return an appropriate response message
                return response()->json(ResponseUtil::makeError($msg));
            } else {
                // redirect back to the page that caused the error
                return redirect()
                    ->back()
                    ->withErrors(['error' => $msg])
                    ->withInput();
            }
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param Request  $request
     * @param AuthenticationException $exception
     * @return Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(ResponseUtil::makeError("Unauthenticated."), 401);
        }

        return redirect()->guest(route('login'));
    }
}
