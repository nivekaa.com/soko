<?php

namespace App\Repositories;

use App\Models\Folder;
use App\Presenters\FolderPresenter;
use App\Repositories\BaseRepository;

/**
 * Class FolderRepository
 * @package App\Repositories
 * @version March 14, 2020, 2:12 pm UTC
*/

class FolderRepository extends BaseRepository
{


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'path',
        'parent_id',
        'owner_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Folder::class;
    }

    public function presenter()
    {
        return FolderPresenter::class;
    }
}
