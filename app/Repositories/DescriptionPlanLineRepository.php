<?php

namespace App\Repositories;

use App\Models\DescriptionPlanLine;
use App\Repositories\BaseRepository;

/**
 * Class DescriptionPlanLineRepository
 * @package App\Repositories
 * @version March 13, 2020, 11:30 pm UTC
*/

class DescriptionPlanLineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'desc',
        'plan_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DescriptionPlanLine::class;
    }
}
