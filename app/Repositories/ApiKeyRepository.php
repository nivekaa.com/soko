<?php

namespace App\Repositories;

use Nivekaa\LaravelApiKey\Models\ApiKey;

/**
 * Class ApiKeyRepository
 * @package App\Repositories
 * @version March 14, 2020, 3:29 am UTC
*/

class ApiKeyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'key',
        'active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApiKey::class;
    }
}
