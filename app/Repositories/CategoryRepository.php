<?php

namespace App\Repositories;

use App\Models\Category;
use App\Presenters\CategoryPresenter;
use App\Transformers\CategoryTransformer;

/**
 * Class CategoryRepository
 * @package App\Repositories
 * @version March 13, 2020, 4:45 pm UTC
*/

class CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Category::class;
    }

    public function presenter()
    {
        return CategoryPresenter::class;
    }


}
