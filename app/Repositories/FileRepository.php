<?php

namespace App\Repositories;

use App\Models\File;
use App\Presenters\FilePresenter;
use App\Repositories\BaseRepository;

/**
 * Class FileRepository
 * @package App\Repositories
 * @version March 15, 2020, 11:38 am UTC
*/

class FileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'extension',
        'type',
        'size',
        'filename',
        'mime_type',
        'category_id',
        'url',
        'owner_id',
        'dirname',
        'folder_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return File::class;
    }

    public function presenter()
    {
        return FilePresenter::class;
    }
}
