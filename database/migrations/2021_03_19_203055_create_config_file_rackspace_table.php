<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileRackspaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_file_rackspace', function (Blueprint $table) {
            $table->id();
            $table->string("rackspace_key_id");
            $table->string("rackspace_username");
            $table->string("rackspace_region");
            $table->string("rackspace_container");
            $table->integer("user_id");
            $table->integer("config_file_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file_rackspace');
    }
}
