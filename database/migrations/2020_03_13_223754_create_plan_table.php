<?php

use App\Enums\InternalPlan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan', function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->float("price")->default(0.0);
            $table->float("promotional_price")->default(0.0);
            $table->enum('interval', [InternalPlan::YEAR,InternalPlan::MONTH, InternalPlan::WEEK])->default(InternalPlan::MONTH);
            $table->integer('interval_count')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan');
    }
}
