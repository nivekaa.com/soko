<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriber_plan', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('plan_id')
                ->references('id')
                ->on('plan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('plan_description_line', function (Blueprint $table) {
            $table->foreign('plan_id')
                ->references('id')
                ->on('plan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('user_api', function (Blueprint $table) {
            $table->foreign('api_id')
                ->references('id')
                ->on('api_keys')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('config_file', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriber_plan', function (Blueprint $table) {
            $table->dropForeign('subscriber_plan_user_id_foreign');
            $table->dropForeign('subscriber_plan_plan_id_foreign');
        });
        Schema::table('plan_description_line', function (Blueprint $table) {
            $table->dropForeign('plan_description_line_plan_id_foreign');
        });
        Schema::table('user_api', function (Blueprint $table) {
            $table->dropForeign('user_api_api_id_foreign');
            $table->dropForeign('user_api_user_id_foreign');
        });
        Schema::table('config_file', function (Blueprint $table) {
            $table->dropForeign('config_file_user_id_foreign');
        });
    }
}
