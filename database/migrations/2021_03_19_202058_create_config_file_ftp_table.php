<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileFtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_file_ftp', function (Blueprint $table) {
            $table->id();
            $table->string("ftp_hostname");
            $table->integer("ftp_port")->default(21);
            $table->string("ftp_username");
            $table->string("ftp_password");
            $table->string("ftp_directory");
            $table->boolean("passive")->default(false);
            $table->boolean("ssl")->default(false);
            $table->integer("user_id");
            $table->integer("config_file_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file_ftp');
    }
}
