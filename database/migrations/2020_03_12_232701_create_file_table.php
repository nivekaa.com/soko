<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("extension",10)->nullable();
            $table->string("type", 24)->nullable();
            $table->integer("size")->nullable();
            $table->string("filename",32);
            $table->string("mime_type",32)->nullable();
            $table->string("category_id")->nullable();
            $table->string("url");
            $table->string("dirname",24);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
