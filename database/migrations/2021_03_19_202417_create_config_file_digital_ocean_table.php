<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileDigitalOceanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_file_digital_ocean', function (Blueprint $table) {
            $table->id();
            $table->string("do_key");
            $table->string("do_secret");
            $table->string("do_region");
            $table->string("do_bucket");
            $table->integer("user_id");
            $table->integer("config_file_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file_digital_ocean');
    }
}
