<?php

use App\Enums\Config\DefaultViewMode;
use App\Enums\Config\FileDeliveryOptimization;
use App\Enums\Config\UserUploadsStorageMethod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /**
     * Configure how user drive should behave.
     */

    public function up()
    {
        Schema::create('config_file', function (Blueprint $table) {
            $table->id();
            $table->enum("dirve_default_view_mode", [DefaultViewMode::GRID, DefaultViewMode::LLIST])
                ->default(DefaultViewMode::GRID)->nullable(); // Default View Mode
            $table->string("localization_timezone", 32)->default("UTC")->nullable();
            $table->string("localization_language")->default("en")->nullable();
            $table->string("localization_date_format")->default("y-M-d")->nullable();
            /**
             * User Uploads
             * Configure size and type of files that users are able to upload.
             * This will affect all uploads across the site.
             *
             */
            $table->enum("user_uploads_storage_method",
                [UserUploadsStorageMethod::LOCAL_DISK, UserUploadsStorageMethod::S3, UserUploadsStorageMethod::FTP,
                    UserUploadsStorageMethod::DIGITAL_OCEAN, UserUploadsStorageMethod::DROPBOX, UserUploadsStorageMethod::RACKSPACE,
                    UserUploadsStorageMethod::BACKBLAZE])->default(UserUploadsStorageMethod::LOCAL_DISK); //User Uploads Storage Method
            $table->enum("file_delivery_optimization", [FileDeliveryOptimization::XACCEL, FileDeliveryOptimization::XSENDFILE]);
            /**
             * When enabled larger files will be uploaded in smaller chunks
             * to improve upload reliability and avoid server max upload size limits
             */
            $table->boolean("chunked_uploads")->default(false);
            $table->boolean("encrypted_file")->default(true);
            // Maximum size for a single file user can upload.
            $table->integer("maximum_file_size")->default(5); // in MB
            // Disk space each user uploads are allowed to take up. This can be overridden per user.
            $table->float("available_space")->default(25)
                ->comment("Par default (25Go)."); // in GB
            $table->text("allowed_file_types")->nullable();
            $table->text("blocked_file_types")->default("exe,js,x-dosexec,application/x-msdownload")
                ->nullable();
            $table->integer("user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file');
    }
}
