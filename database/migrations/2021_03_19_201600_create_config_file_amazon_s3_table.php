<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileAmazonS3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_file_amazon_s3', function (Blueprint $table) {
            $table->id();
            $table->string("as3_key");
            $table->string("as3_secret");
            $table->string("as3_region")->default("us-east-1")->nullable();
            $table->string("as3_bucket");
            $table->string("as3_endpoint")->nullable();
            $table->integer("user_id");
            $table->integer("config_file_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file_amazon_s3');
    }
}
