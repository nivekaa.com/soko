<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFileBackblazeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_file_backblaze', function (Blueprint $table) {
            $table->id();
            $table->string("backblaze_key_id");
            $table->string("backblaze_application_key");
            $table->string("backblaze_region")->default("us-west-002");
            $table->string("backblaze_bucket_name");
            $table->integer("user_id");
            $table->integer("config_file_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_file_backblaze');
    }
}
