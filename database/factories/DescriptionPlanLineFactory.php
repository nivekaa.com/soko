<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DescriptionPlanLine;
use Faker\Generator as Faker;

$factory->define(DescriptionPlanLine::class, function (Faker $faker) {

    return [
        'value' => $faker->word,
        'desc' => $faker->word,
        'plan_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
