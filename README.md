# SOKO Files Storage

[![N|Solid](http://soko.isjetokoss.xyz/images/logo.svg)](https://nodesource.com/products/nsolid)
&nbsp;

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://soko.io) 

Soko est un serveur en permettant de stocker les fichiers par téléchargement via les Services RESTs.
  - Upload des fichiers
  - téléchargement des fichiers uploadés
  - Creation des dossiers et sous-dossiers

# New Features!
  - Importer un fichier
  - suprimer un fichier
  - Importer un fichier dans un dossier
  - ajouter/suprimer des dossiers et sous-dossiers

## APIs
###### Base URL for using the service: `https://soko.io/storage/api/`
###### HTTP methods: `POST, GET, PUT, PATCH, DELETE`
###### Content Type: `application/json, multipart/form-data, application/x-www-form-data`
&nbsp;
##### Détails fournis par le service qui utilise l'API
- #### header
    > Content-Type : ${content type}
    X-API-KEY: ${value}

&nbsp;
___________
## API Services Reference
____________
&nbsp;&nbsp;
&nbsp;
#### **Categories de fichiers**
__________
- **listing**
    __Description__: Get all file categorie
    __Method__: `GET`
    __URL__: `http://soko.io/storage/api/v1/categories/`
    __Request__: 
    ```sh
    curl -X GET "http://soko.io/storage/api/v1/categories" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire:** ID en path.
    **Response Example:**
    http://soko.io/storage/api/v1/categories
    ```json
    {
      "success": true,
      "presents": {
        "data": [
          {
            "name": "conpress"
          },
          {
            "name": "Image"
          },
          {
            "name": "Video"
          }
        ]
      },
      "message": null
    }
    ```

- **show**
    __Description__: Get specific category
    __Method__: `GET`
    __URL__: `http://soko.io/storage/api/v1/categories/`CAT_ID`/`
    __Request__: 

    ```sh
    curl -X GET "http://soko.io/storage/api/v1/categories/{CATEGORIE_ID}" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```

    **Parametres obligatoire: pas de parametres.
    Response Example:**
    http://soko.io/storage/api/v1/categories/1
    
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "name": "conpress"
        }
      },
      "message": null
    }
    ```
&nbsp;
#### **fichiers**
_____
- Get All
__Description__: Get all Files with pagination. Max files per page is 25
__Parameters__: `page: Page number. Default is 1`
__Method__: `GET`
__URL__: `http://soko.io/storage/api/v1/files/`
__Request__: 
    ```sh
    curl -X GET "http://soko.io/storage/api/v1/files?page=[PAGE]" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire: pade de parametre obligatoire.
Response Example:**
http://soko.io/storage/api/v1/files?page=1
    ```json
    {
      "success": true,
      "presents": {
        "data": [
          {
            "extension": "pdf",
            "type": "file",
            "size": "79.782 KB",
            "original_name": "ab1c176206eb425590597d2ab9e637b9.pdf",
            "url": "http://localhost:8000/soko//ab1c176206eb425590597d2ab9e637b9.pdf",
            "created_at": "2020-03-20 02:12:06",
            "folder": {
              "name": "test"
            },
            "created_by": {
              "name": "soko adminer"
            }
          },
          {
            "extension": "png",
            "type": "file",
            "size": "5.866 KB",
            "original_name": "ac0141a228094724a3d0c5a7247a7f30.png",
            "url": "http://soko.io/soko/2/sub-test/ac0141a228094724a3d0c5a7247a7f30.png",
            "created_at": "2020-03-18 22:30:01",
            "folder": {
              "name": "sub-test"
            },
            "created_by": {
              "name": "soko adminer"
            }
          }
        ],
        "meta": {
          "pagination": {
            "total": 17,
            "count": 17,
            "per_page": 25,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
          }
        }
      },
      "message": null
    }
    ```

- Add
__Description__: `Store new file`
__Parameters__:
    - file* : binary form-data
    - folder* : name or ID of folder
    
    __Method__: `POST`
    __Content-Type__: `multipart/form-data`
    __URL__: `http://soko.io/storage/api/v1/files`
    __Request__: 
    ```sh
    curl -X POST "http://soko.io/storage/api/v1/files" -H "accept: application/json" -H "X-API-KEY: [APIKEY]" -H "Content-Type: multipart/form-data" -F "file=@[FILE_NAME];type=[FILE_MIME_TYPE]" -F "folder=[FILE_ID_OR_UUID]"
    ```
    **Parametres obligatoire**: `file, folder`.
    **Response Example:**
    http://soko.io/storage/api/v1/files
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "extension": "",
          "type": "file",
          "size": "11.075 KB",
          "original_name": "06ef81e0ee6f42428946880e2bb4b960",
          "url": "http://soko.io/soko/06ef81e0ee6f42428946880e2bb4b960",
          "created_at": "2020-03-21 21:35:53",
          "folder": {
            "name": "test"
          },
          "created_by": {
            "name": "soko adminer"
          }
        }
      },
      "message": "File saved successfully"
    }
    ```

- Add file basee64
__Description__: `Store new file encoded in base64`
__Parameters__:
    - file* : encoded file
    - folder* : name or ID of folder
    
    __Method__: `POST`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/files/store/base64`
    __Request__: 
    ```sh
    curl -X POST "http://soko.io/storage/api/v1/files/store/base64" -H "accept: application/json" -H "X-API-KEY: [APIKEY]" -H "Content-Type: application/json" -d "{ \"file\": \"[BASE_64_STRING]\", \"folder\": \"[FOLDER]\"}"
    ```
- Get file
__Description__: `Get File. The $id can be :
    * UUID File ID
    * or file name : http://soko.io/soko/..ANOTHER_WAY../{filename} (filename: e.g: dqsdqsdqe.png, aezrzerzer.pdf, lkfkmzoj.txt, etc...)
    * or path : http://soko.io/soko/{path} here the path variable must be base64 encoded. This is cause of / may contain the PATH,
    * or the orginal filename (e.g: foo.txt, foo bar.pdf, etc...)
    
    __Path__: id* (File id)
    __Method__: `GET`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/files/`{FILE_ID}`/`
    __Request__: 
    ```sh
    curl -X GET "http://soko.io/storage/api/v1/files/{FILE_ID}" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire**: `id dans l'url`.
**Response Example:**
http://soko.io/storage/api/v1/files/9bb30be7-1f2d-4bff-b62d-caafd03fcf0b
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "extension": "pdf",
          "type": "document",
          "size": "11.075 KB",
          "original_name": "le con.pdf",
          "url": "http://soko.io/soko/06ef81e0ee6f42428946880e2bb4b960.pdf",
          "created_at": "2020-03-21 21:35:53",
          "folder": {
            "name": "pragmatics"
          },
          "created_by": {
            "name": "soko adminer"
          }
        }
      },
      "message": ""
    }
    ```

- Delete file
__Description__: `Remove the specified File from storage`
__Parameters__:
    - file* : encoded file
    - folder* : name or ID of folder
    
    __Method__: `DELETE`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/files/`ID`/`
    __Request__: 
    ```sh
    curl -X DELETE "http://soko.io/storage/api/v1/files/{ID}" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire**: `id ou nom ou path(base64) dans l'url`.
    **Response Example:**
    http://soko.io/storage/api/v1/files/9bb30behLKH8D878Djhkjqslkjsqu78DHK==
    ```json
    {
      "success": false,
      "message": "File not found"
    }
    ```


- Show files
__Description__: `Retrieved the files in specified folder`
    Get Files. The `$folder` can be :
    * UUID Folder ID
    * or folder name
    * or path : http://soko.io/soko/{path}/{...files_contening...} here the path variable must be base64 encoded. This is cause of / may contain the PATH,
    * or the orginal filename (e.g: foo.txt, foo bar.pdf, etc...)
    with pagination. Max files per page is 25
    
    __Parameters__:
    - folder* : encoded file
    - folder* : name or ID of folder
    
    __Method__: `GET`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/files/`ID`/`
    __Request__: 
    ```sh
    curl -X DELETE "http://soko.io/storage/api/v1/files/{ID}" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire**: `id ou nom ou path(base64) dans l'url`.
    **Response Example:**
    http://soko.io/storage/api/v1/files/9bb30behLKH8D878Djhkjqslkjsqu78DHK==
    ```json
    {
      "success": false,
      "message": "File not found"
    }
    ```

&nbsp;
#### **Dossiers**
_____
- **listing**
    __Method__: `GET`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/folders/`
    __Request__: 
    ```sh
    curl -X GET "http://soko.io/storage/api/v1/folders" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire:** aucun parametre
    **Response Example:**
    http://soko.io/storage/api/v1/folders
    ```json
    {
      "success": true,
      "presents": {
        "data": [
          {
            "_id": "7fd3c287-1711-4a3d-866d-7762eb1ecb53",
            "name": "sub-test",
            "base": false,
            "created_at": "2020-03-18 09:35:30",
            "created_by": {
              "name": "soko adminer"
            },
            "parent": {
              "_id": "9d99929c-fab1-4c69-91e1-e54993fbca33",
              "name": "test"
            }
          },
          {
            "_id": "8faf735e-db7b-4eb2-a78b-9a1e684e90d6",
            "name": "paratics",
            "base": false,
            "created_at": "2020-03-14 20:31:12",
            "created_by": {
              "name": "soko adminer"
            },
            "parent": null
          }
        ],
        "meta": {
          "pagination": {
            "total": 8,
            "count": 8,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
          }
        }
      },
      "message": "Folders retrieved successfully"
    }
    ```

- Add folder
__Description__: `Store new folder`
__Parameters__:
    - name* : name of folder
    - parent* : parent name or UUID
    
    __Method__: `POST`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/folders`
    __Request__: 
    ```sh
    curl -X POST "http://soko.io/storage/api/v1/folders" -H "accept: application/json" -H "X-API-KEY: [APIKEY]" -H "Content-Type: application/json" -d "{ \"name\": \"[NAME]\", \"parent\": \"[PARENT]\"}"
    ```
    **Parametres obligatoire:** 
    -  name
    
    **Response Example:**
    http://soko.io/storage/api/v1/folders
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "_id": "63264c77-da93-4565-bccc-89a372acd483",
          "name": "xxxxx",
          "base": null,
          "created_at": "2020-03-21 23:32:10",
          "created_by": {
            "name": "soko adminer"
          },
          "parent": null
        }
      },
      "message": "Folder saved successfully"
    }
    ```

- Delete folder 
__Description__: `Remove the specified Folder from storage`
__Path__:
    - id* : folder ID
    
    __Method__: `DELETE`
    __Content-Type__: `json`
    __URL__: `http://soko.io/storage/api/v1/folder/`ID`/`
    __Request__: 
    ```sh
    curl -X DELETE "http://soko.io/storage/api/v1/folders/{ID}" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire**: `id ou nom ou path(base64) dans l'url`.
    **Response Example:**
    http://soko.io/storage/api/v1/folders/xdffsdfs
    ```json
    {
      "success": false,
      "message": "Folder not found"
    }
    ```
&nbsp;
#### **Registration**
___
- Get user
    __Description__: `Get user information`
    __Method__: `GET`
    __Content-Type__: `application/json`
    __URL__: `http://soko.io/storage/api/v1/user`
    __Request__: 
    ```sh
    curl -X DELETE "http://soko.io/storage/api/v1/user" -H "accept: application/json" -H "X-API-KEY: [APIKEY]"
    ```
    **Parametres obligatoire**: none.
    **Response Example:**
    http://soko.io/storage/api/v1/user
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "id": 2,
          "name": "soko adminer",
          "email": "admin@soko.io",
          "max_api": 3,
          "space_left": 10,
          "number_files": 300,
          "created_at": "2020-03-13 10:59:00",
          "updated_at": "2020-03-20 08:46:02"
        }
      },
      "message": null
    }
    ```
- Updated user information
    __Description__: `Update user information`
    __Method__: `PUT`
    __Paremeter__: 
    - name: user name

    __Content-Type__: `application/json`
    __URL__: `http://soko.io/storage/api/v1/user/update`
    __Request__: 
    ```sh
    curl -X PUT "http://soko.io/storage/api/v1/user/update" -H "accept: application/json" -H "X-API-KEY: [APIKEY]" -H "Content-Type: application/json" -d "{ \"name\": \"[user_name]\"}"
    ```
    **Parametres obligatoire**: none.
    **Response Example:**
    http://soko.io/storage/api/v1/user/update
    ```json
    {
      "success": true,
      "presents": {
        "data": {
          "id": 2,
          "name": "soko admin updated",
          "email": "admin@soko.com",
          "max_api": 3,
          "space_left": 10,
          "number_files": 300,
          "created_at": "2020-03-13 10:59:00",
          "updated_at": "2020-03-22 00:05:38"
        }
      },
      "message": null
    }
    ```
    
&nbsp;
## Models
____

- ##### File
| Name | Type | Required? | Description |
| ------ | ------ | ------ | ------ |
| id | UUID(string) | no | Identifier |
| name  | string(64) | yes | name of file |
| extension | string(16) | no | the extension of file (e.g: png, zip, txt, etct...) |
| type | String(16) | no | type of file (e.g: file, image, zip, etct...) |
| size | string\|integer | no | the size for file |
| filename | string\|integer | no | the file name |
| mime_type | string | no | mime type (e.g: application/zip, image/png, etc...) |
| url | string | no | full path |
| owner_id | integer | no | owner ID: the current user by API KEY |
| folder | string | yes | folder UUID id or name or path |
|=======|======|addictional while displaying|======|
| created_dy | objet#User | no | The object user who created file |
| folder | objet#Folder | no | The object folder considerated as parent folder |

- ##### Folder

| Name | Type | Required? | Description |
| ------ | ------ | ------ | ------ |
| id | UUID(string) | no | Identifier |
| name  | string(64) | yes | name of folder |
| folder_id | string | no | parent id or parent name folder |
| is_base | boolean | no | soon |
|=======|======|addictional while displaying|======|
| created_dy | objet#User | no | The object user who created file |
| folder | objet#Folder | no | The object folder considerated as parent folder |

&nbsp;
_____
## License

__Sôko files__ is open-sourced software licensed under the [Apache-2.0 license](https://opensource.org/licenses/Apache-2.0).


