<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::middleware(["verified"])->group(function (){
    Route::get('/home', 'HomeController@index')->name("home");
    Route::resource('categories', 'CategoryController');
    Route::resource('users', 'UserController');
    Route::resource('plans', 'PlanController');
    Route::resource('descriptionPlanLines', 'DescriptionPlanLineController');
    Route::resource('apiKeys', 'ApiKeyController');
    Route::post('apikey/new', 'ApiKeyController@store');
    Route::put('apikey/active/{id}', 'ApiKeyController@active')->name("apikey.active");


});

Route::get('pricing', 'SubscriptionController@pricing');


Route::get("doc_md", function (\Illuminate\Http\Request $request){
  $readmeFile = file_get_contents(base_path("README.md"));
  // dd($readmeFile);
  return view("md", ["md"=>$readmeFile]);
});
