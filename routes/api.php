<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::middleware(['auth:api'])
    ->group(function () {
        Route::apiResource('apiKeys', 'ApiKeyAPIController');
        Route::post('apikey/new', 'ApiKeyAPIController@store');
        Route::put('apikey/active/{id}', 'ApiKeyAPIController@active')->name("apikey.active");
    });

Route::middleware(['auth:api'])->get('/user', function (Request $request) {
        return $request->user();
    });

Route::group(["prefix"=>"v1"],function (){
    Route::middleware(["auth.apikey","auth.header"])->group(function (){
        Route::get('categories/{id}', 'CategoryAPIController@show');
        Route::get('categories', 'CategoryAPIController@index');
        Route::get('users/{id}', 'UserAPIController@show')->name("users.show");
        Route::put('user/update', 'UserAPIController@update')->name("user.update");
        Route::get('user', 'UserAPIController@user')->name("user.info");
        Route::get('plans/{id}', 'PlanAPIController@show');
        //folder resources
        Route::get('folders', 'FolderAPIController@index');
        Route::post('folders', 'FolderAPIController@store');
        Route::get('folders/{id}', 'FolderAPIController@show');
        Route::put('folders/{id}', 'FolderAPIController@update');
        Route::delete('folders/{id}', 'FolderAPIController@destroy');

        // files resources
        Route::get('files', 'FileAPIController@index');
        Route::post('files', 'FileAPIController@store');
        Route::get('files/{id}', 'FileAPIController@show');
        Route::put('files/{id}', 'FileAPIController@update');
        Route::delete('files/{id}', 'FileAPIController@destroy');

        Route::post('files/store/base64', 'FileAPIController@storeBase64');
        Route::post('files/multi/store', 'FileAPIController@storeMultiple');
        Route::get('files/folder/{folder}/show', 'FileAPIController@showInFolder');
    });
});


