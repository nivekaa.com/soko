
@extends('layouts.app')

@section('css')
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">

    <!-- Datatables css -->
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css">
    <!-- style css -->
@endsection

@section('title')
@endsection
@section('breadcrump')
@endsection
@section('content')
    <!-- data table start -->
    <div class="col-12 mt-2">
        <div class="card">
            <div class="card-body">
                <h3 class="header-title">Categories</h3>
                <div class="data-tables">
                    <table class="table table-bordered table-hover" id="categories">
                        <thead>
                        <th>Name</th>
                        <th style="width: 20px">Action</th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>
                                    {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        <a href="{{ route('categories.edit', [$category->id]) }}" class='btn btn-primary btn-xs mb-3'>Edit</a>
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs mb-3"> Delete</button>
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- data table end -->
@endsection
@section('scripts')
    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>

    <script type="application/javascript">
      $(function () {
        $(document).ready(function () {
          $('#categories').DataTable();
        });
      });
    </script>
@endsection
