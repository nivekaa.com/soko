
@extends('layouts.app')

@section('css')
@endsection

@section('title')
@endsection
@section('breadcrump')
@endsection
@section('content')
   <!-- Input Group start -->
        <div class="col-12">
           <div class="card mt-5">
               <div class="card-body">
                   @include('adminlte-templates::common.errors')
                   <h4 class="header-title"> Category</h4>
                   {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'patch']) !!}

                       @include('categories.fields')

                  {!! Form::close() !!}
                </div>
           </div>
       </div>
       <!-- Input Group end -->
@endsection

@section('scripts')
<script>

</script>
@endsection
