<!-- Name Field -->
<div class="form-group has-primary">
    <label for="name" class="col-form-label">name</label>
    <input type="text" value="{{old('name', null)}}" name="name" class="form-control form-control-primary" id="name" placeholder="name">
    <div class="form-control-feedback"></div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-default">Cancel</a>
</div>
