@extends('layouts.app')

@section('css')
@endsection

@section('title')
    Home - SOKO
@endsection
@section('breadcrump')
@endsection
@section('content')
    <!-- trading history area start -->
    <div class="row mt-2 mr-2 ml-1" style="width: 100%">
        <div class="col-12">
            @include('vendor.flash.message')
            @include('adminlte-templates::common.errors')
        </div>
    </div>
    <div class="row mb-3 mr-2 ml-1 mt-1">
        <div class="col-xl-9 col-ml-8 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-between align-items-center">
                        <h4 class="header-title">Manage ApiKey History</h4>
                        <div class="trd-history-tabs">
                            <ul class="nav" role="tablist">
                                <li>
                                    <a class="active" data-toggle="tab" href="#buy_order" role="tab">API Event</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#sell_order" role="tab">List API Key</a>
                                </li>
                            </ul>
                        </div>
                        <button data-toggle="modal" data-target="#newApiKey" class="btn btn-sm btn-primary border-0 pr-3">Add new &nbsp;&nbsp; <i class="fa fa-plus-circle"></i></button>
                        <div class="modal fade" id="newApiKey">
                            <form action="{{url("apikey/new")}}" method="POST">
                                <input type="hidden" name="user_id" value="{{auth()->id()}}"/>
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">create an new key</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name" class="col-form-label">Name</label>
                                                <input name="name" class="form-control" placeholder="Enter a valide name" type="text" id="name">
                                                <div class="text-danger"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary btn-xs">create</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="trad-history mt-4">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="buy_order" role="tabpanel">
                                <div class="table-light">
                                    <table class="dbkit-table">
                                        <tr class="heading-td">
                                            <td>API NAME</td>
                                            <td>API KEY</td>
                                            <td>STATUS</td>
                                            <td>CREATD AT</td>
                                        </tr>
                                        @foreach($apis as $api)
                                            <tr>
                                                <td>{{$api->name}}</td>
                                                <td data-ak="{{$api->key}}">
                                                    <button onclick="copyToClipBoard(this);" class="btn btn-outline-info btn-xs">
                                                        copy
                                                    </button>
                                                </td>
                                                <td>{!! $api->active?'<div class="text-success"><i class="fa fa-check"></i></div>':'<div class="text-denger"><i class="fa fa-close"></i></div>' !!}</td>
                                                <td>{{$api->created_at->format("Y.m.d")}}</td>
                                                <td class="text-right">
                                                    <div class="btn-toolbar" style="display: inline-flex!important;">
                                                        {{Form::open(["route" => ['apikey.active', $api->id], 'method' => 'put'])}}
                                                            <input type="hidden" name="active" value="{{$api->active?0:1}}"/>
                                                            <input type="hidden" name="user_id" value="{{auth()->id()}}"/>
                                                            <button type="submit" class="btn btn-xs btn-sm mx-1 btn-{{$api->active?'warning':'success'}}">{{$api->active?'unlock':'Lock'}}</button>
                                                        {{Form::close()}}
                                                        {{Form::open(["route"=> ['apiKeys.destroy', $api->id], 'method' => 'delete'])}}
                                                            <button type="submit" class="btn btn-xs btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i> </button>
                                                        {{Form::close()}}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sell_order" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="dbkit-table">
                                        <tr class="heading-td">
                                            <td>Trading ID</td>
                                            <td>Time</td>
                                            <td>Status</td>
                                            <td>Amount</td>
                                            <td>Last Trade</td>
                                        </tr>
                                        <tr>
                                            <td>78211</td>
                                            <td>4.00 AM</td>
                                            <td>Pending</td>
                                            <td>$758.90</td>
                                            <td>$05245.090</td>
                                        </tr>
                                        <tr>
                                            <td>782782</td>
                                            <td>4.00 AM</td>
                                            <td>Pending</td>
                                            <td>$77878.90</td>
                                            <td>$7778.090</td>
                                        </tr>
                                        <tr>
                                            <td>89675978</td>
                                            <td>4.00 AM</td>
                                            <td>Pending</td>
                                            <td>$0768.90</td>
                                            <td>$0945.090</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- trading history area end -->
        <!-- timeline area start -->
        <div class="col-xl-3 col-ml-4 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Timeline Event</h4>
                    <div class="timeline-area">
                        <div class="timeline-task">
                            <div class="icon bg1">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="tm-title">
                                <h4>Rashed sent you an email</h4>
                                <span class="time"><i class="ti-time"></i>09:35</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
                            </p>
                        </div>
                        <div class="timeline-task">
                            <div class="icon bg2">
                                <i class="fa fa-exclamation-triangle"></i>
                            </div>
                            <div class="tm-title">
                                <h4>Rashed sent you an email</h4>
                                <span class="time"><i class="ti-time"></i>09:35</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
                            </p>
                        </div>
                        <div class="timeline-task">
                            <div class="icon bg2">
                                <i class="fa fa-exclamation-triangle"></i>
                            </div>
                            <div class="tm-title">
                                <h4>Rashed sent you an email</h4>
                                <span class="time"><i class="ti-time"></i>09:35</span>
                            </div>
                        </div>
                        <div class="timeline-task">
                            <div class="icon bg3">
                                <i class="fa fa-bomb"></i>
                            </div>
                            <div class="tm-title">
                                <h4>Rashed sent you an email</h4>
                                <span class="time"><i class="ti-time"></i>09:35</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
                            </p>
                        </div>
                        <div class="timeline-task">
                            <div class="icon bg3">
                                <i class="ti-signal"></i>
                            </div>
                            <div class="tm-title">
                                <h4>Rashed sent you an email</h4>
                                <span class="time"><i class="ti-time"></i>09:35</span>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse distinctio itaque at.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- timeline area end -->
    </div>
@endsection

@section('scripts')
    <script>
        function copyToClipBoard(e) {
          let parent = $(e).parent();
          var text = parent.attr("data-ak");
          navigator.clipboard.writeText(text).then(function() {
            console.log('Async: Copying to clipboard was successful!');
            $(e).html("copied &nbsp;&nbsp; <i class='fa fa-check'></i>");
            $(e).addClass("text-success","btn-outline-success");
          }, function(err) {
            console.error('Async: Could not copy text: ', err);
            $(e).removeClass("text-success");
          });
        }
    </script>
@endsection
