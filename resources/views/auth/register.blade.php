@extends('layouts.app_auth')

@section('content')
<div class="login-area login-s2">
    <div class="container">
        <div class="login-box ptb--100">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="login-form-head">
                    <h4>{{config("app.name")}}</h4>
                    <p>Hello there, Sign up and Join with Us</p>
                </div>
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="exampleInputName1">{{ __('Name') }}</label>
                        <input type="text" required name="name" class="@error('name') is-invalid @enderror" id="exampleInputName1">
                        <i class="ti-user"></i>
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputEmail1">{{ __('E-Mail Address') }}</label>
                        <input type="email" required name="email" class="@error('email') is-invalid @enderror" id="exampleInputEmail1">
                        <i class="ti-email"></i>
                        @error('email')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">{{ __('Password') }}</label>
                        <input type="password" required name="password" class="@error('password') is-invalid @enderror" id="exampleInputPassword1">
                        <i class="ti-lock"></i>
                        @error('password')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword2">{{ __('Confirm Password') }}</label>
                        <input type="password" id="exampleInputPassword2" name="password_confirmation" required {{--autocomplete="new-password"--}}>
                        <i class="ti-lock"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">{{ __('Register') }} <i class="ti-arrow-right"></i></button>
                        <div class="login-other row mt-4" style="display: none">
                            <div class="col-6">
                                <a class="fb-login" href="#">Sign up with <i class="fa fa-facebook"></i></a>
                            </div>
                            <div class="col-6">
                                <a class="google-login" href="#">Sign up with <i class="fa fa-google"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Don't have an account? <a href="{{url("login")}}">{{ __("Login")  }}</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
