@extends('layouts.app_auth')

@section('content')
<div class="login-area">
    <div class="container">
        <div class="login-box ptb--100">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="login-form-head">
                    <h4>{{config("app.name")}}</h4>
                    <p>Hello there, Sign in and start managing your Files Storage</p>
                </div>
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="exampleInputEmail1">{{ __('E-Mail Address') }}</label>
                        <input type="email" name="email" class="@error('email') is-invalid @enderror" id="exampleInputEmail1">
                        <i class="ti-email"></i>
                        @error('email')
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">{{ __('Password') }}</label>
                        <input type="password" class="@error('password') is-invalid @enderror" name="password" id="exampleInputPassword1">
                        <i class="ti-lock"></i>
                        @error('password')
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="row mb-4 rmber-area">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing" {{ old('remember') ? 'checked' : '' }}>

                                <label class="custom-control-label" for="customControlAutosizing" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                        <div class="col-6 text-right">
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="submit-btn-area">
                        <button class="fb-login" id="form_submit" type="submit">{{__("Login")}} <i class="ti-arrow-right"></i></button>
                        <div class="login-other row mt-4" style="display: none">
                            <div class="col-6">
                                <a class="fb-login" href="#">Log in with <i class="fa fa-facebook"></i></a>
                            </div>
                            <div class="col-6">
                                <a class="google-login" href="#">Log in with <i class="fa fa-google"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Don't have an account? <a href="{{url("register")}}">Sign up</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
