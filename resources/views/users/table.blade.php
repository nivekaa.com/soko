<div class="data-tables datatable-primary">
    <table class="text-left" id="users-table">
        <thead class="text-capitalize">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Verified</th>
                <th>Space Left</th>
                <th>Number Files</th>
                <th style="width: 10%"></th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{!! !empty($user->email_verified_at)?"<i class='fa fa-check text-success'></i>":"<i class='fa fa-close text-danger'></i>" !!}</td>
                <td>{{ $user->space_left }} Go</td>
                <td>{{ $user->number_files }}</td>
                <td class="text-right">
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('users.edit', [$user->id]) }}" class='btn btn-info btn-xs mb-1'>Edit</a>
                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs mb-1"> Delete</button>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push("addic-scripts")
<script>
    $('#users-table').DataTable({
      responsive: true,
      columnDefs: [
        { orderable: false, targets: -1 }
      ]
    });
    </script>
@endpush
