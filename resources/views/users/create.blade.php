
@extends('layouts.app')

@section('css')
@endsection

@section('title')
@endsection
@section('breadcrump')
@endsection
@section('content')
    <!-- Input Group start -->
     <div class="col-12">
        <div class="card mt-5">
            <div class="card-body">
                @include('adminlte-templates::common.errors')
                <h4 class="header-title"> User</h4>
                {!! Form::open(['route' => 'users.store']) !!}

                    @include('users.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- Input Group end -->
@endsection

@section('scripts')
<script>

</script>
@endsection
