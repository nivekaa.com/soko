<!-- Name Field -->
<div class="form-group has-primary">
    <label for="name" class="col-form-label">name</label>
    <input type="text" value="{{old('name', isset($user)?$user->name:null)}}" name="name" class="form-control form-control-primary" id="name" placeholder="name">
    <div class="form-control-feedback"></div>
</div>


<!-- Email Field -->
<div class="form-group has-primary">
    <label for="email" class="col-form-label">Email</label>
    <input type="email" value="{{old('email', isset($user)?$user->email:null)}}" name="email" class="form-control form-control-primary" id="email" placeholder="name@example.com">
    <div class="form-control-feedback"></div>
</div>

<!-- Number Files Field -->
<div class="form-group has-primary">
    <label for="number_files" class="col-form-label">number_files</label>
    <input type="number" value="{{old('number_files', isset($user)?$user->number_files:null)}}" name="number_files" class="form-control form-control-primary" id="number_files" placeholder="number_files">
    <div class="form-control-feedback"></div>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
</div>
